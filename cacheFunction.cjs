function cacheFunction(cb){
    if(!cb){
        throw new Error('No parameters passed');
    }
    let cacheObject = {};
    
    return (...args) => {
        const argsString = JSON.stringify(args);
        if(argsString in cacheObject){
            return cacheObject[argsString];
        }
        else{
            const result = cb(...args)
            cacheObject[argsString] = result;
            return result;
        }
    }
}

module.exports = cacheFunction;