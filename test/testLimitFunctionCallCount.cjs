let limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function add(a,b){
    return a+b;
}

const cbExample = limitFunctionCallCount(add,2);

console.log(cbExample(5,6));
console.log(cbExample(4,5));
console.log(cbExample(8,9));

try{
    const cbExample2 = limitFunctionCallCount(add);
    console.log(cbExample2(2,5));
}
catch(e){
    console.error(e);
}

try{
    const cbExample3 = limitFunctionCallCount();
    console.log(cbExample3(2,5));
}
catch(e){
    console.error(e);
}