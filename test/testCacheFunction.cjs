let cacheFunction = require('../cacheFunction.cjs');

function add(a,b){
    console.log('Executed add function');
    return a+b;
}

const cbExample = cacheFunction(add);

console.log(cbExample(3,4));
console.log(cbExample(3,4));
console.log(cbExample(5,6));

try{
    const cbExample2 = cacheFunction();
    console.log(cbExample2(4,5));
}
catch(e){
    console.error(e);
}