function limitFunctionCallCount(cb,n){
    if(!cb){
        throw new Error('No parameters passed');
    }
    if(!n){
        throw new Error('limit not passed');
    }
    let counter = 0;
    return (...args) => counter++<n ? cb(...args) : null;
}

module.exports = limitFunctionCallCount;