function counterFactory(){
    let closureVariable = 0;
    function increment(){
        return ++closureVariable;
    }
    function decrement(){
        return --closureVariable;
    }
    return {increment, decrement};
}

module.exports = counterFactory;